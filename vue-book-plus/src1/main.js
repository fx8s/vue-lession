//vuex评级组建的交互：找共同的父级组件进行解决  2、跨组件交互：eventBus(发布订阅，可能会造成逻辑混乱，命名冲突等问题)

//vuex主要借鉴了 flux redux（这两种可以在任意环境中使用）
//vuex只能在vue中使用  ，应用于大型项目 ，主要是应用于状态（数据）管理，将数据统一管理

import Vue from 'vue'
import App from './App.vue'

import store from './store/index'

//做一个计数器
new Vue({
  el:"#app",
  ...App,
  store
});
