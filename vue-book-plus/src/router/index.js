import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// import Home from '../components/Home'
// import Collect from '../components/Collect'
// import Add from '../components/Add'
// import Detail from '../components/Detail'
// import List from '../components/List'

export default new Router({
  routes: [
    // 路由元信息官方文档 https://router.vuejs.org/zh/guide/advanced/meta.html
    {path: '/', redirect: "/home"},
    {path: '/home', component: () => import('../components/Home'), meta: {keepAlive: true,title:'首页'}},  //this.$route.meta.keepAlive  //是否需要缓存
    {path: '/collect', component: () => import('../components/Collect'),meta: {title:'收藏'}},
    {path: '/add', component: () => import('../components/Add'),meta: {title:'添加'}},
    {path: '/detail/:bid', component: () => import('../components/Detail'), name: 'detail',meta: {title:'详情'}},
    {path: '/list', component: () => import('../components/List'),meta: {title:'列表'}},
    {path: '*', redirect: "/home"},
  ]
})
