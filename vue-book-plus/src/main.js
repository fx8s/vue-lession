// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueLazyload from 'vue-lazyload'

// or with options
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: 'http://pic.51yuansu.com/pic3/cover/01/37/31/59268849b8585_610.jpg',
  loading: 'https://images2017.cnblogs.com/blog/785246/201709/785246-20170905110009585-1158296496.gif',
  attempt: 1
})

//导入轮播图插件
import VueAwesomeSwiper from 'vue-awesome-swiper'

Vue.config.productionTip = false
import 'swiper/dist/css/swiper.css'

//使用轮播图插件
Vue.use(VueAwesomeSwiper, /* { default global options } */)

Vue.config.productionTip = false

// 在进入路由之前，每一次都会执行此方法
//全局钩子
router.beforeEach(function (to, from, next) {

  document.title = from.meta.title

  // if(to.path === '/list'){
  //   next({path:'/add'})
  // }

  next()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
})
