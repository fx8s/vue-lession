let http = require('http');
let fs = require('fs');
let url = require('url');

let sliders = require('./sliders')

function read(cb) {
  fs.readFile('./mock/book.json', 'utf8', function (err, data) {
    if (err || data.length == 0) {
      console.log(err);
      cb([]); // 错误或者没长度返回空数组
    } else {
      cb(JSON.parse(data)); // 将读出来的数据转换成对象
    }
  });

}

// 写入内容
function write(data, cb) {
  fs.writeFile('./mock/book.json', JSON.stringify(data), cb);
}


// read(function (books) { //获得所有图书
//   console.log(books)
// });

let pageSize = 5; //每页显示5个

http.createServer((req, res) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
    res.setHeader("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.setHeader("X-Powered-By", ' 3.2.1')


    console.log(req.method);

    if (req.method == "OPTIONS") return res.end();/*让options请求快速返回*/

    // if (req.methods == "OPTIONS") {
    //   return res.end();  //让options请求快速返回
    // }

    let {pathname, query} = url.parse(req.url, true);  //需要加true，query才是对象


    if (pathname === '/page') {
      let offset = parseInt(query.offset) || 0;
      console.log(offset);
      read(function (books) {
        //每次偏移量再基础上加5个
        let result = books.slice(offset, offset + pageSize);
        console.log(offset, pageSize);
        let hasMore = true;
        if (books.length <= offset + pageSize) {  //显示的数目大于显示条目
          hasMore = false;
        }

        res.setHeader('Content-Type', 'application/json;charset=utf8');
        setTimeout(()=>{
          res.end(JSON.stringify({hasMore, books: result}));
        },500)


      });
      return;


    }

    if (pathname === '/sliders') {
      res.setHeader('Content-Type', 'application/json;charset=utf8');
      res.end(JSON.stringify(sliders));
    }

    if (pathname === '/hot') {
      read(function (books) {
        let hot = books.reverse().slice(0, 6);
        res.setHeader('Content-Type', 'application/json;charset=utf8');
        setTimeout(() => {
          res.end(JSON.stringify(hot));
        }, 500);

      });

      return;
    }

    if (pathname === '/book') { //对书的增删改查
      let id = parseInt(query.id);  //取出的是字符串

      switch (req.method) { // ?id=1
        case "GET":
          if (!isNaN(id)) { //有id，就是查询一个
            read(function (books) {
              let book = books.find(item => item.bookId === id);
              if (!book) {
                book = {}; //如果没找到则是undefined
              }
              res.setHeader('Content-Type', 'application/json;charset=utf8');
              res.end(JSON.stringify(book));
            })
          } else {  // 获取所有图书
            read(function (books) {
              res.setHeader('Content-Type', 'application/json;charset=utf8');
              res.end(JSON.stringify(books.reverse()))
            })
          }
          break;
        case "POST":
          let str = '';
          req.on('data', chunk => {
            str += chunk
          });
          req.on('end', () => {
            console.log('str  ---- ', str)
            let book = JSON.parse(str);
            read(function (books) { // 添加id
              book.bookId = books.length ? books[books.length - 1].bookId + 1 : 1;
              books.push(book); //将数据放到books中 ，books在内存中
              write(books, function () {
                res.end(JSON.stringify(book));
              });
            });
          });
          break;
        case "PUT":
          console.log('put111');
          if (id) {// 获取了当前要修改的id
            let str = '';
            req.on('data', chunk => {
              str += chunk;
            });
            req.on('end', () => {
              let book = JSON.parse(str);//book要改成什么样子
              read(function (books) {
                books = books.map(item => {
                  if (item.bookId === id) { // 找到id相同的那一本书
                    return book
                  }
                  return item; // 其他书正常返回即可
                });
                write(books, function () { // 将数据写回json中
                  res.setHeader('Content-Type', 'application/json;charset=utf8');
                  res.end(JSON.stringify(book));
                })
              });
            })
          }
          break;
        case "DELETE":
          read(function (books) {
            books = books.filter(item => item.bookId !== id)
            write(books, function () {
              res.setHeader('Content-Type', 'application/json;charset=utf8');
              res.end(JSON.stringify({}));
            });
          });
          break;
      }
      return;
    }

    // fs.stat(pathname,function (err,stats) {
    //   if(err){
    //     res.statusCode = 404;
    //     res.end('not found')
    //   }else{  //目录会报错
    //
    //
    //     if(stats.isDirectory()){
    //       let p = require('path').join('./vue-book-plus/mock/'+pathname,'index.html')
    //       fs.createReadStream(p).pipe(res);
    //     }else{
    //       fs.createReadStream('./vue-book-plus/mock/'+pathname).pipe(res);
    //     }
    //
  //   }
    //
    //
    // });
  }).listen(3000)
