import modal from './notify.vue'

let notify = {  //需要在此对象中拥有一个install方法
    //只显示一个

};
notify.install = function (Vue, options = {delay: 3000}) {
    Vue.prototype.$notify = function (message, opt = {}) {

        if (notify.el) {
            return;
        }

        options = {...options, ...opt};  //用自己调用插件时传递过来的属性 覆盖默认的默认设置的属性
        let V = Vue.extend(modal);    //返回的是一个构造函数的子类，参数是包含组件选项的对象
        let vm = new V;
        let oDiv = document.createElement('div');   //创建一个div将实例挂在到元素上

        vm.$mount(oDiv);    //挂载
        vm.value = message;

        notify.el = vm.$el;

        document.body.appendChild(vm.$el);  //加入到div上

        setTimeout(() => {   //延迟多少秒 将dom元素移除
                document.body.removeChild(vm.$el);  //将实例上的元素删除
                notify.el = null;
            }, options.delay
        );
    }
};

// 导出这个包含install的对象，如果使用Vue.use就会调用这个install方法
export default notify;