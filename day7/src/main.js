import Vue from 'vue';
// 直接引用的vue，引用的是runtime
// vue = compiler + runtime(compiler 可以编译模板)
//runtime 不支持 template 只支持render


//导入组件
import App from './App.vue'

import router from './router/index.js'

import notify from './plugin/notify.js'

Vue.use(notify,{
    delay:2000
});

new Vue({
    router,
    el: '#app',
    render: h => h(App)
})

