import axios from 'axios'
import qs from 'qs'



const instance = axios.create({
  baseURL: 'http://localhost:3000',
  headers:{
    'Content-Type':'application/x-www-form-urlencoded'
  },
  transformRequest: [function (data) {
    data = qs.stringify(data);
    return data;
  }],
  withCredentials:true   //加了这段就可以跨域了
});

//增加默认的请求路径
axios.defaults.baseURL = 'http://localhost:3000';

//增加拦截器
axios.interceptors.response.use((res) => {
  return res.data;  //在这里统一拦截结果，把结果处理成res.data
})

// axios.interceptors.request.use((req) =>{
//
// });



// 添加请求拦截器
// https://blog.csdn.net/ljj1338/article/details/81812035
// axios.interceptors.request.use(function (config) {
//   // 参数格式转换
//   if (config.method == "post") {
//     config.data = qs.stringify(config.data);
//   }
//   return config;
// }, function (error) {
//   // 对请求错误做些什么
//   return Promise.reject(error);
// });



// 获取轮播图数据,返回的是一个promise对象
export let getSliders = () => {
  return axios.get('/sliders')
};

//获取热门图书
export let getHotBook = () => {
  return axios.get("/hot");
};

//获取全部图书
export let getBooks = () => {
  return axios.get("/book");
};
//删除
export let removeBook = (id) => {
  return axios.delete(`/book?id=${id}`);
};

//获取某一本书
export let findOneBook = (id) => {
  return axios.get(`/book?id=${id}`);
};

//修改图书
/**
 * @param id 编号
 * @param data 数据 请求体发送
 * @returns {AxiosPromise<T>}
 */
export let updateBook = (id, data) => {
  return axios.put(`/book?id=${id}`, data);
};
/**
 * 添加
 * @param id
 * @param data
 * @returns {AxiosPromise<any>}
 */
export let addBook = (data) => {

  // return axios.post('/book',
  //   qs.stringify({
  //     bookName: data.bookName,
  //     bookInfo: data.bookInfo
  //   }))
  //   .then(function (response) {
  //     console.log(response);
  //   })
  //   .catch(function (error) {
  //     console.log(error);
  //
  //   });

  return axios.post(`/book`, data);

  // return axios.post('/book',
  //   qs.stringify(data))
  //   .then(function (response) {
  //     console.log(response);
  //   })
  //   .catch(function (error) {
  //     console.log(error);
  //
  //   });

};


