import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Home from '../components/Home'
import Collect from '../components/Collect'
import Add from '../components/Add'
import Detail from '../components/Detail'
import List from '../components/List'

export default new Router({
  routes: [
    {path:'/',redirect:"/home"},
    {path:'/home',component:Home},
    {path:'/collect',component:Collect},
    {path:'/add',component:Add},
    {path:'/detail/:bid',component:Detail,name:'detail'},
    {path:'/list',component:List},
    {path:'*',redirect:"/home"},
  ]
})
