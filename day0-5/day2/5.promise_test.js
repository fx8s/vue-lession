function buy(callback) {
  setTimeout(() => {
      let a = '蘑菇';
      callback(a);
    }, 2000
  )
}

buy(function cookie(a) {
  console.log(a)
});

//事情做好后掉

//promise 解决回调问题 promise 三个状态 成功态，失败态，等待

console.log(Promise)

//resolve 代表转向成功态
//reject 代表转向失败态 resolve 和 reject均是函数
//promise实例就有then方法，then方法中有两个参数
let p = new Promise((resolve, reject) => {
  setTimeout(() => {
      let a = '鸡腿';
      resolve(a);
    }, 4000
  )
});
console.log(2)

p.then((data) => {
  console.log(data)
}, (err) => {
  console.log('err')
})
