function ajax({
                url = '',
                type = 'get',
                dataType = 'json'
              }) {
  return new Promise((resolve, reject) => {

    let xhr = new XMLHttpRequest();
    xhr.open(type, url, true);
    xhr.responseType = dataType;
    xhr.onload = function () {  //xhr.readState = 4,xhr.status = 200
      if(xhr.status === 200){
        resolve(xhr.response);  //成功调用
      }else{
        reject('not found');  //失败
      }

    };
    xhr.onerror = function(err){
      reject(err);  //失败
    };
    xhr.send();
  })

}

