let arr = [1,2,3,4,5]
arr.b = "aaa"
for(let i =0;i<arr.length;i++){
    console.log(arr[i]);
}

arr.forEach(function (item) {
    console.log(item);
});
console.log("箭头函数 --------------");
arr.forEach(item => console.log(item))

console.log("箭头函数 完--------------");
for(let key in arr){
    console.log(key);
}
console.log("--------------");
for (let val of arr){
    console.log(val);
}

console.log("--------------");


let obj = {school:'test',age:8}
for (let val of Object.keys(obj)){
    console.log(obj[val]);
}

//filter
let newArr = [1,2,3,4,5].filter(function (item) {
    return item >2 && item<5;
});

console.log(newArr);

//map
let arr1 = [1,2,3].map(function (item) {
   return `<li>${item}</li>`;
});
console.log(arr1);
console.log(arr1.join(''));

//includes 是否包含
let arr3 = [1,2,3,4,55]
console.log(arr3.includes(5));  //boolean

//5)find 返回找到的那一项，不会改变数组 回调函数中返回true表示找到了，找到后停止循环，找不到返回的是undefined
let result = arr3.find(function (item, index) {
    return item == 4
})

console.log(result);
console.log(typeof(result));

//6)some 找true 找到true后停止返回true，找不到返回false
//7)every 找false 找到false 停止返回false
//8)reduce 收敛 4个参数，返回叠加后的方法
console.log("8 reduce");
//prev,数组第一项,//next，数组第二项
let sum = [1,2,3,4,5].reduce(function (prev,next,index,item) {
    return prev + next;
});
console.log(sum);

let sum2 = [{price:30,count:2},{price:30,count:3},{price:30,count:4}].reduce(function (prev,next) {
   return prev+ next.price * next.count
},0);

console.log(sum2);

//
let newArr2 = [[1,2,3,],[4,5,6],[7,8,9]].reduce(function (prev,next) {
    return prev.concat(next)
});

console.log(newArr2)


