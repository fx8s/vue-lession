//箭头函数
//自己家没有this 就找上一级 this

//如何更改this
//1)call apply bind
//2)var that = this;
//3) => 箭头函数

//如何确定this是谁，看.前面是谁this就是谁
// function a(b) {
//    return b+1;
// }
//
// let a = b => b+1;   //去掉function 关键字，参数只有一个可以省略小括号 小括号和大括号之间有一个箭头，如果没有大括号则直接返回值 有大括号必须写return
// function a(b) {
//     return function (c) {
//         return b +c;
//     }
// }

let a = b => c => b+c;  //高阶函数 （>=两个箭头的）

console.log(a(1)(2))

//闭包:函数执行的一瞬间叫闭包，不销毁的作用域
//当执行后返回的结果是引用数据类型，被外界变量接收 此时这个函数不会销毁(模块化)
let arr = [1,2,3,4,5];
arr.forEach(item => console.log(item))

var num = 1;

var str = '1';

var test = 1;

console.log("test == num",test == num); //true　相同类型　相同值
console.log("test === num",test === num); //true　相同类型　相同值
console.log("test !== num",test !== num); //false test与num类型相同，其值也相同,　非运算肯定是false









