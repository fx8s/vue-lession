//webpack必须采用commonjs写法
let path = require('path');
let HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  entry: './src/main.js',  //打包入口文件，webpack会自动查找相关依赖进行打包
  output: {
    filename: 'bundle.js', //打包后的名字
    path: path.resolve('./dist') //必须是一个绝对路径
  },
  //模块的解析规则
  // - js 匹配所有的js 用babel-loader 转译 排除掉node_modules
  module:{
    rules:[
      {test:/\.js$/,use:'babel-loader',exclude:/node_modules/},
      //use时从右往左写
      {test:/\.css$/,use:['style-loader','css-loader']},
      {test:/\.less$/,use:['style-loader','css-loader','less-loader']},
      //8k以下不使用base64
      {test:/\.(jpg|png|gif|jpeg)/,use:'url-loader?limit=8192'},
      {test:/\.(eot|svg|woff|woff2|wtf)/,use:'url-loader'},
      {test:/\.vue$/,use:'vue-loader'}


    ]
  },
  plugins:[
    new HtmlWebpackPlugin({
      template: './src/index.html',
      // filename: 'login.html'
    })
  ]
};
