## 模块
- node 模块的标准 commonjs
- cmd seajs amd require
- esmodule
- umd 为了做兼容处理的
- esmodule
  - 如何定义模块（一个js就是一个模块）
  - 如何导出模块（export）
  - 如何使用模块（import）

## babel 转译 es6 -> es5

```
  npm install babel-core --save-dev
  npm install babel-loader --save-dev
```

## babel-preset-es2015
```
  npm install babel-preset-es2015 --save-dev 
```

## babel-preset-stage-0
-0 包含1,2,3,4是es6
```
  npm install babel-preset-stage-0 --save-dev
```

## 解析样式
- css-loader将css解析成模块，使用style-loader 插入到
```
  npm install css-loader style-loader --save-dev
```

## less,sass,stylus(预处理语言)
- less-loader less css-loader style-loader
- sass-loader 
- stylus-loader
```
  npm install less less-loader  --save-dev
```

## 解析图片
- file-loader url-loader(依赖于file-loader)
```
  npm install file-loader url-loader --save-dev
```

## 需要解析html插件
- 插件的作用是以我们自己的html为模板将打包后的结果，自动引入到html中产出到dist目录下面
```
  npm install html-webpack-plugin --save-dev
```

## webpack-dev-server
- 这里面内置了服务，可以帮我们启动一个端口号，当代码更新时，可以自动打包（内存打包），代码有变换就重新执行
```
  npm install webpack-dev-server --save-dev
```

## 建立工程
```
    npm install webpack webpack-dev-server babel-core babel-loader babel-preset-es2015 babel-preset-stage-0 css-loader style-loader less less-loader file-loader url-loader html-webpack-plugin --save-dev
```

## 安装vue-loader vue-template-compiler
- vue-loader 解析.vue文件的
- vue-template-compiler 用来解析vue模板的
```
  npm install vue-loader vue-template-compiler --save-dev
```
