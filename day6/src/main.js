import Vue from 'vue';
// 直接引用的vue，引用的是runtime
// vue = compiler + runtime(compiler 可以编译模板)

import App from './App.vue'
console.log(App)

new Vue({
  el: '#app',
  render:function (createElement) {
    return createElement(App)
  }
})

