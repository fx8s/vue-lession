//如果是第三方模块，不需要加./ 如果是文件模块需要加./
//在另一个文件中将内容解构出来才能使用
//import 有声明功能
// console.log(str); //没报错
// import {str,str2,f} from './a.js'
import * as b from './a.js'
import xxx from './b.js'

console.log(b.str,b.str2)
// f();

// b.f();

console.log(xxx);
